﻿using App.ViewModel;

namespace App.Pages;

public partial class ChampionsListPage : ContentPage
{
    public ChampionListPageVM ChampionListPageVM { get; private set; }

	public ChampionsListPage(ChampionListPageVM championListPageVM)
	{
        ChampionListPageVM = championListPageVM;
        ChampionListPageVM.Navigation = Navigation;
        InitializeComponent();
		BindingContext = ChampionListPageVM;
    }
}