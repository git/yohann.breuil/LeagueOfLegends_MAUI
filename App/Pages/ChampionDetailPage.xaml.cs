﻿using App.ViewModel;
using ViewModel;

namespace App.Pages;

public partial class ChampionDetailPage : ContentPage
{
    #region Properties

    public ChampionDetailPageVM ChampionDetailPageVM { get; private set; }
    public ChampionManagerVM ChampionManagerVM { get; private set; }
    public ChampionVM ChampionVM { get; private set; }

    #endregion

    public ChampionDetailPage(ChampionDetailPageVM championDetailPageVM, ChampionManagerVM championManagerVM)
	{
        ChampionDetailPageVM = championDetailPageVM;
        ChampionManagerVM = championManagerVM;
        ChampionVM = ChampionManagerVM.SelectedChampion;

        ChampionDetailPageVM.Navigation = Navigation;
        InitializeComponent();
        BindingContext = this;
    }
}