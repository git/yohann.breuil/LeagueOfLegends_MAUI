﻿using System;
using System.Windows.Input;
using ViewModel;
using App.Pages;

namespace App.ViewModel
{
	public class ChampionDetailPageVM 
	{
        #region Properties

        public INavigation Navigation { get; set; }

        #endregion

        #region Commands

        public ICommand NaviagteToChampionAddEditPageCommand { get; private set; }

        #endregion

        public ChampionDetailPageVM()
		{
            NaviagteToChampionAddEditPageCommand = new Command(
                execute: async () => await Navigation.PushModalAsync(new ChampionAddEditPage()),
                canExecute: () => Navigation is not null
            );  
		}
	}
}

