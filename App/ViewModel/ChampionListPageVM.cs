﻿using System.Windows.Input;
using App.Pages;
using ViewModel;

namespace App.ViewModel
{
	public class ChampionListPageVM
	{
        #region Properties

        public INavigation Navigation { get; set; }
        public ChampionManagerVM ChampionManagerVM { get; private set; }

        #endregion

        #region Commands

        public ICommand NavigateToChampionDetailCommand { get; private set; }
        public ICommand NaviagteToChampionAddEditPageCommand { get; private set; }

        #endregion

        public ChampionListPageVM(ChampionManagerVM championManager)
        {
            ChampionManagerVM = championManager;

            NavigateToChampionDetailCommand = new Command<ChampionVM>(
                execute: async (selectedChampion) =>
                {
                    ChampionManagerVM.SelectedChampion = selectedChampion;
                    await Shell.Current.GoToAsync(nameof(ChampionDetailPage));
                },
                canExecute: (selectedChampion) => ChampionManagerVM is not null && selectedChampion is not null
            );

            NaviagteToChampionAddEditPageCommand = new Command<ChampionVM>(
                execute: async (selectedChampion) => await Navigation.PushModalAsync(new ChampionAddEditPage()),
                canExecute: (selectedChampion) => ChampionManagerVM is not null && Navigation is not null
            );
        }
    }
}