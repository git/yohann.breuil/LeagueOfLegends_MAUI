﻿using System.Globalization;

namespace App.Converters
{
	public class Base64ToImage : IValueConverter
	{
		public Base64ToImage()
		{
            
		}

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var bytes = System.Convert.FromBase64String(value as string);
            return ImageSource.FromStream(() => new MemoryStream(bytes));
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}

