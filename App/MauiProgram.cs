﻿using Microsoft.Extensions.Logging;
using CommunityToolkit.Maui;
using Model;
using StubLib;
using ViewModel;
using App.Pages;
using App.ViewModel;

namespace App;

public static class MauiProgram
{
	public static MauiApp CreateMauiApp()
	{
		var builder = MauiApp.CreateBuilder();
		builder
			.UseMauiApp<App>()
            .UseMauiCommunityToolkit()
            .ConfigureFonts(fonts =>
			{
				fonts.AddFont("OpenSans-Regular.ttf", "OpenSansRegular");
				fonts.AddFont("OpenSans-Semibold.ttf", "OpenSansSemibold");
			})
			.Services.AddSingleton<IDataManager, StubData>()
            .AddSingleton<ChampionListPageVM>()
			.AddTransient<ChampionDetailPageVM>()
            .AddSingleton<ChampionManagerVM>()
            .AddSingleton<ChampionsListPage>()
			.AddSingleton<ChampionDetailPage>();			

#if DEBUG
		builder.Logging.AddDebug();
#endif

		return builder.Build();
	}
}

