﻿using App.Pages;

namespace App;

public partial class AppShell : Shell
{
	public AppShell()
	{
		InitializeComponent();

        Routing.RegisterRoute(nameof(ChampionDetailPage), typeof(ChampionDetailPage));
    }
}

