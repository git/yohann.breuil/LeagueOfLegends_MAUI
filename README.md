# League of Legends

## 🎯 Goal

Create a MVVM League of Legends application in .NET MAUI with a home MVVM and MVVM Toolkit 

## 💻 Development 

* Branch master => Contains home MVVM
* Branch mvvm-toolkit => Contains MVVM Toolkit 

## 💡 Explanation

### Home MVVM

#### MVVM

> Model View View Model

Assemblies : 

* App
	* View : 
		* Write in XAML
		* Represents app views
		* Data Bindings between XAML element properties and ViewModel properties
		* Events thrown by XAML element
	* App VM : 
		* Write in C#
		* Manage all user interactions with view (navigation, pagination, buttons, ...) with Commands
		* Dialog between View and ViewModel
* ViewModel
	* ViewModel : 
		* Write in C#
		* Wrap / Encapsulate model
		* Wrap / Encapsulate model properties
		* Wrap / Encapsulate model methods with Commands
		* Notify model from update (PropertyChanged and CollectionChanged)
* Model 
	* Model 
		* Write in C#
		* Represents business logic
		* Adapted for different applications (Web, Mobile, API, ...)

	
#### League of Legend MVVVM

##### ChampionsListPage

BindingContext on ChampionsListPageVM

Classes : 

* ChampionsListPageVM
	* Contain ChampionsManagerVM property
	* Manage navigation with Commands

* ChampionsManagerVM 
	* Wrap champions collection
	* Wrap data manager
	* Load champions
	* Manage pagination with index, count, nbTotalPage (properties) and previous / next page (commands)
	* Contain selected champion

##### ChampionDetailPage

BindingContext on this (ChampionDetailPage)

Use in ChampionDetailPage initialization : 

* ChampionVM (from selected champion of ChampionManagerVM) for Champion properties
* ChampionDetailPageVM for navigation

Classes : 

* ChampionVM
	* Wrap Champion model
	* Wrap Chamion properties 

* ChampionDetailPageVM
	* Manage navigation with Commands

##### ChampionAddEditPage

Not done

##### Dependency Injection

In this app, I use dependency injection on :

* ChampionsListPage - Singleton
* ChampionsManagerVM - Singleton
* ChampionDetailPage - Transient
* IDataManager => StubData - Singeton

### MVVM Toolkit

Not done

### Fonctionalities

* Load champions list and display it : Work
* Paginate champions list and go to previous / next page : Work
* Select champion : Work with a bug
* Display champion detail in detail page : Work
* Add a champion : Not done
* Edit a champion : Not done

## 📝 Documentation 

* [.NET MAUI](https://learn.microsoft.com/en-us/dotnet/maui/?WT.mc_id=dotnet-35129-website)
* [.NET MAUI Community Toolkit](https://learn.microsoft.com/en-us/dotnet/communitytoolkit/maui/)
* [.NET MAUI MVVM Toolkit](https://learn.microsoft.com/en-us/dotnet/communitytoolkit/mvvm/)

## 👨‍💻 Author

**BREUIL Yohann**

* GitHub: [@DJYohann](https://github.com/DJYohann)
* LinkedIn: [@BREUIL Yohann](https://www.linkedin.com/in/yohann-breuil-02b18a165/)