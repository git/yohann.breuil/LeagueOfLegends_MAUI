﻿using System;
using Model;

namespace ViewModel
{
	public class ChampionVM : BaseToolkit
	{
        public Champion Model
        {
            get => _model;
            set
            {
                if (_model.Equals(value) || value == null) return;
                _model = value;
                OnPropertyChanged();
            }
        }
        private Champion _model;

        public string Name
        {
            get => Model.Name;
        }

        public string Bio
        {
            get => Model.Bio;
            set
            {
                if (value == null) return;
                _model.Bio = value;
                OnPropertyChanged();
            }
        }

        public string Icon
        {
            get => Model.Icon;
            set
            {
                if (value == null) return;
                _model.Icon = value;
                OnPropertyChanged();
            }
        }

        public string Image
        {
            get => Model.Image.Base64;
            set
            {
                if (value == null) return;
                _model.Image.Base64 = value;
                OnPropertyChanged();
            }
        }

        public IReadOnlyDictionary<string, int> Characteristics
        {
            get => Model.Characteristics;
        }

        public ChampionVM(Champion model)
        {
            _model = model;
        }

        public ChampionVM()
        {
            _model = new Champion("Poppy", ChampionClass.Tank);
        }
    }
}

