﻿using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace ViewModel
{
    public class BaseToolkit
    {
        public event PropertyChangedEventHandler? PropertyChanged;

        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}

