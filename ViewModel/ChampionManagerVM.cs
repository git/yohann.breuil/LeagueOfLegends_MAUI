﻿using System;
using Model;
using System.Collections.ObjectModel;
using System.Windows.Input;

namespace ViewModel
{
	public class ChampionManagerVM : BaseToolkit
	{
        #region Properties

        public ReadOnlyObservableCollection<ChampionVM> ChampionVMs { get; private set; }
        private ObservableCollection<ChampionVM> _championVMs { get; set; } = new ObservableCollection<ChampionVM>();

        public IDataManager DataManager
        {
            get => _dataManager;
            set
            {
                if (_dataManager == value) return;
                _dataManager = value;
                OnPropertyChanged();
                LoadChampions(Index, Count);
            }
        }
        private IDataManager _dataManager;

        public int Index
        {
            get => _index;
            set
            {
                if (_index == value) return;
                _index = value;
                OnPropertyChanged();
                (NextPageCommand as Command).ChangeCanExecute();
                (PreviousPageCommand as Command).ChangeCanExecute();
            }

        }
        private int _index;

        public int Count { get; set; } = 5;

        public ChampionVM SelectedChampion { get; set; }

        #endregion

        #region Commands

        public ICommand LoadChampionsCommand { get; private set; }
        public ICommand NextPageCommand { get; private set; }
        public ICommand PreviousPageCommand { get; private set; }

        #endregion

        #region Constructors

        public ChampionManagerVM(IDataManager dataManager)
        {
            DataManager = dataManager;
            ChampionVMs = new ReadOnlyObservableCollection<ChampionVM>(_championVMs);
            PropertyChanged += ChampionManagerVM_PropertyChanged;

            LoadChampionsCommand = new Command(
                execute: async () => await LoadChampions(Index, Count),
                canExecute: () => DataManager is not null
                );

            NextPageCommand = new Command(
                execute: () => NextPage(),
                canExecute: () => CanNextPage()
                );

            PreviousPageCommand = new Command(
                execute: () => PreviousPage(),
                canExecute: () => CanPreviousPage()
                );
        }

        #endregion

        private async void ChampionManagerVM_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (e.PropertyName == nameof(Index))
                await LoadChampions(Index, Count);
        }

        public int NbPages => NbChampions / Count;

        public int NbChampions
        {
            get => _nbChampions;
            set
            {
                _nbChampions = value;
            }
        }
        private int _nbChampions;

        private async Task GetNbChampions()
        {
            NbChampions = await DataManager.ChampionsMgr.GetNbItems();
            OnPropertyChanged(nameof(NbPages));
        }

        #region Commands methods

        private async Task LoadChampions(int index, int count)
        {
            _championVMs.Clear();
            var champions = await DataManager.ChampionsMgr.GetItems(index, count);
            foreach (var champion in champions)
            {
                _championVMs.Add(new ChampionVM(champion));
            }
        }

        private void NextPage()
        {
            Index += 1;
        }

        private bool CanNextPage()
        {
            return true;
        }


        private void PreviousPage()
        {
            Index -= 1;
        }

        private bool CanPreviousPage()
        {
            return Index > 0;
        }
   
        #endregion
    }
}

